﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using Grpc.Core;
using TestAgent.Utilities;

using Command;
using FileUploader;
using System.Linq;
using System.Threading;
using System.Net;

namespace TestAgent.Client
{
    public static class SendRPC
    {
        const string hostIPAddress = "192.168.0.7";
        const string host = "192.168.0.7:50051"; // "localhost:50051"

        private static IPAddress m_IPAddress;
        private static IPAddress ipAddress
        {
            get
            {
                if (m_IPAddress == null)
                {
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(hostIPAddress);
                    m_IPAddress = ipHostInfo.AddressList[0];
                }
                return m_IPAddress;
            }
        }

        public static async Task UpdateServer()
        {
            var channel = new Channel(host, SslCredentials.Insecure);

            // Upload server files to temp
            await UploadFiles(Paths.ServerSourcePath, Paths.TempDownload, channel);

            // Upload updater files
            await UploadFiles(Paths.ServerUpdaterSourcePath, Paths.ServerUpdaterPath, channel);

            // Trigger Update Command
            await Command(CommandRequest.Types.Type.ServerUpdate);
            channel.ShutdownAsync().Wait();
        }

        private static async Task UploadFiles(string sourcePath, string destinationPath, Channel channel)
        {
            // Get all files from folder
            var sourceFiles = Directory.GetFiles(sourcePath);

            // Upload files async and wait until done
            foreach (var sourceFile in sourceFiles)
                await UploadFile(sourceFile, destinationPath, channel);
        }

        public static async Task<CommandResponse> Command(CommandRequest.Types.Type type)
        {
            var response = new CommandResponse();
            var channel = new Channel(host, SslCredentials.Insecure);
            var client = new CommandService.CommandServiceClient(channel);

            try
            {
                response = await client.CommandAsync(new CommandRequest { Type = type });
            }
            catch (RpcException e)
            {
                response.Message = e.Message;
            }

            channel.ShutdownAsync().Wait();

            return response;
        }

        public static async Task<string> UploadFile(string fileName, string destination, Channel channel)
        {
            if (!File.Exists(fileName))
                return "File not found.";

            var response = new UploadResponse();
            Console.WriteLine("Create channel");
            Console.WriteLine("Create client");
            var client = new FileUploader.UploadService.UploadServiceClient(channel);

            try
            {
                Console.WriteLine("Send response");
                var filePath = Path.Combine(destination, Path.GetFileName(fileName));
                var responseAsync = client.UploadAsync(new UploadRequest { FileName = filePath, FileSize = 0 });

                //Open Socket and upload file content
                Console.WriteLine("Open socket");
                SocketUtility.OpenSocket((Socket socket) =>
                {
                    Console.WriteLine("Sending file...");
                    socket.SendFile(fileName);
                }, ipAddress: ipAddress);

                // Wait server to receive the file...
                Console.WriteLine("Wait for response");
                response = await responseAsync;
                Console.WriteLine("Response received");

                //response = await responseAsync;
                Console.WriteLine("Channel closed");
            }
            catch (RpcException e)
            {
                Console.WriteLine(e.Message);
                response.Message = e.Message;
            }

            return response.Message;
        }
    }
}
