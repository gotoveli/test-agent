﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TestAgent.Utilities
{
    // State object for reading client data asynchronously  
    public class StateObject
    {
        public System.Type type;
        // Callbacks
        public SocketUtility.SocketAction socketOpenedCallback;
        public ManualResetEvent done = new ManualResetEvent(false);
        // Client  socket.  
        public Socket socket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024 * 16;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }
}
