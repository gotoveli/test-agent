﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TestAgent.Utilities
{
    public static class ProcessUtility
    {
        public static void RunProcess(string path, string arguments = "", bool waitForExit = true)
        {
            try
            {
                Console.WriteLine("Run Process: " + path + " " + arguments);

                using (var process = new Process())
                {
                    var startInfo = new ProcessStartInfo
                    {
                        FileName = path,
                        Arguments = arguments,
                        UseShellExecute = false,
                    };

                    process.StartInfo = startInfo;
                    process.Start();

                    if (waitForExit)
                        process.WaitForExit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
