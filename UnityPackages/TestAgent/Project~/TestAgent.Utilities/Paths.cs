﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAgent.Utilities
{
    public static class Paths
    {
        public const string InstallPath = @"C:\TestAgent\";
        public const string Source = @"C:\src\test-agent\UnityPackages\TestAgent\Project~\";

        public const string TempDownload = InstallPath + "Temp";

        public const string ServerUpdaterPath = InstallPath + "Updater";
        public const string ServerUpdaterProcessPath = InstallPath + @"Updater\TestAgent.Updater.exe";
        public const string ServerUpdaterSourcePath = Source + @"TestAgent.Updater\bin\Debug\netcoreapp3.0";

        public const string ServerPath = InstallPath + @"Server";
        public const string ServerProcessPath = InstallPath + @"Server\TestAgent.Server.exe";
        public const string ServerSourcePath = Source + @"TestAgent.Server\bin\Debug\netcoreapp3.0";
    }
}
