﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestAgent.Utilities
{
    public static class SocketUtility
    {
        private const int port = 11000;
        private const int connectionRetries = 2;

        public delegate void SocketAction(Socket socket);
        public delegate void ContentReceivedAction(object content);

        public static void OpenSocket(SocketAction callback, IPAddress ipAddress)
        {
            // Connect to a remote device.  
            try
            {
                //if (string.IsNullOrEmpty(hostNameOrAddress))
                //    hostNameOrAddress = Dns.GetHostName();

                // Establish the remote endpoint for the socket.  
                //Console.WriteLine("ipHostInfo");
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("192.168.0.7");
                //Console.WriteLine("ipAddress");
                //ipAddress = ipHostInfo.AddressList[0];
                Console.WriteLine("remoteEP : " + ipAddress);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                Console.WriteLine("Create TCP/P socket");
                //Thread.Sleep(500);
                // Create a TCP/IP socket.  
                Socket socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                var state = new StateObject()
                {
                    socket = socket,
                    socketOpenedCallback = callback,
                };

                // Connect to the remote endpoint.  
                for (int i = 0; i < connectionRetries; i++)
                {
                    Console.WriteLine("Begin connection");
                    var async = socket.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), state);
                    var succeed = async.AsyncWaitHandle.WaitOne();
                    if (succeed)
                    {
                        state.done.WaitOne();
                        break;
                    }
                }

                Console.WriteLine("Shutdown socket");
                socket.Shutdown(SocketShutdown.Both);
                Console.WriteLine("Close socket");
                socket.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void ListenSocket (SocketAction callback)
        {
            try
            {
                // Establish the local endpoint for the socket.  
                // The DNS name of the computer  
                string hostNameOrAddress = Dns.GetHostName();
                IPHostEntry ipHostInfo = Dns.GetHostEntry(hostNameOrAddress);
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.  
                Socket socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                // Bind the socket to the local endpoint and listen for incoming connections.  
                socket.Bind(localEndPoint);
                socket.Listen(1);

                var state = new StateObject()
                {
                    socket = socket,
                    socketOpenedCallback = callback
                };

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine("Waiting for a connection...");
                var async = socket.BeginAccept(new AsyncCallback(AcceptConnectionCallback), state);
                var succeed = async.AsyncWaitHandle.WaitOne();
                if (succeed)
                    state.done.WaitOne();

                socket.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                var state = (StateObject)ar.AsyncState;
                var socket = state.socket;

                // Complete the connection.
                Console.WriteLine("Accept using EndConnect");
                socket.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}", socket.RemoteEndPoint.ToString());
                Console.WriteLine("Callback");
                state.socketOpenedCallback?.Invoke(socket);
                state.done.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void AcceptConnectionCallback(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject)ar.AsyncState;

                // Accept the connection.  
                var socket = state.socket.EndAccept(ar);
                state.socketOpenedCallback?.Invoke(socket);
                state.done.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
