﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using FileUploader;
using Grpc.Core;
using TestAgent.Utilities;

namespace TestAgent.Server
{
    public class FileUploadService : UploadService.UploadServiceBase
    {
        public override Task<UploadResponse> Upload(UploadRequest request, ServerCallContext context)
        {
            var message = request.FileName;

            //Listen socket for file upload connection
            SocketUtility.ListenSocket((Socket socket) =>
            {
                // Download path
                var path = request.FileName;

                // Donwnload file content
                using (var networkStream = new NetworkStream(socket))
                using (var fileStream = File.Create(path))
                {
                    Console.WriteLine("Receiving File Content...");
                    networkStream.CopyTo(fileStream);
                    Console.WriteLine("File Content Received.");
                }

                // Response
                message = "File saved as: " + path;
            });

            return Task.FromResult(new UploadResponse { Message = message });
        }
    }
}
