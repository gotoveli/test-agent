﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Command;
using Grpc.Core;
using TestAgent.Utilities;

namespace TestAgent.Server
{
    public class CommandService : Command.CommandService.CommandServiceBase
    {
        public override Task<CommandResponse> Command(CommandRequest request, ServerCallContext context)
        {
            var message = "";
            switch (request.Type)
            {
                case CommandRequest.Types.Type.None:
                    break;
                case CommandRequest.Types.Type.RestartOs:
                    //message = RestartOS();
                    break;
                case CommandRequest.Types.Type.ServerUpdate:
                    ServerUpdate();
                    break;
            }

            return Task.FromResult(new CommandResponse { Message = message });
        }

        static void ServerUpdate()
        {
            var processID = Process.GetCurrentProcess().Id;

            var arguments = "update" +
                            " --processid " + processID +
                            " --processpath " + Paths.ServerProcessPath +
                            " --source " + Paths.TempDownload +
                            " --destination " + Paths.ServerPath;

            // Start updater process
            ProcessUtility.RunProcess(Paths.ServerUpdaterProcessPath, arguments, waitForExit: false);

            // Shutdown server...
            Server.Shutdown();
        }

        static void RunProcess()
        {
        }

        static string RestartOS()
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = "shutdown",
                Arguments = "/r",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
            };

            var process = new Process { StartInfo = startInfo };
            process.Start();
            process.WaitForExit();
            return process.StandardOutput.ReadToEnd();
        }
    }
}
