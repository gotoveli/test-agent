using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;
using IApplicationLifetime = Microsoft.Extensions.Hosting.IHostApplicationLifetime;

namespace TestAgent.Server
{
    public class Server
    {
        public static IApplicationLifetime applicationLifetime;

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static void Shutdown()
        {
            Console.WriteLine("Shutting down server...");
            applicationLifetime.StopApplication();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(options =>
                    {
                        options.Limits.MinRequestBodyDataRate = null;
                        options.ListenAnyIP(50051, listenOptions =>
                        {
                            // later we'll use SSL
                            listenOptions.Protocols = HttpProtocols.Http2;
                            listenOptions.NoDelay = true;                            
                        });
                    });

                    webBuilder.UseStartup<Startup>();
                });
    }
}
