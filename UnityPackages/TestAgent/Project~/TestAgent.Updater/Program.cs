﻿using System;
using System.Diagnostics;
using System.IO;
using CommandLine;
using TestAgent.Utilities;

namespace TestAgent.Updater
{
    class Program
    {
        [Verb("update", HelpText = "Updates and restarts a running process.")]
        public class UpdateOptions
        {
            [Option("processid", Required = true, HelpText = "Process ID of running application being updated.")]
            public int ProcessID { get; set; }

            [Option("processpath", Required = true, HelpText = "Path of running application being updated.")]
            public string ApplicationPath { get; set; }

            [Option("source", Required = true, HelpText = "Path of downloaded files.")]
            public string Source { get; set; }

            [Option("destination", Required = true, HelpText = "Path to copy the downloaded files.")]
            public string Destination { get; set; }
        }

        static void Main(string[] args)
        {
            var result = Parser.Default.ParseArguments<UpdateOptions>(args);
            result.MapResult((UpdateOptions o) => Update(o), errs => 1);
        }

        static int Update(UpdateOptions options)
        {
            // Get target process
            var process = GetProcess(options.ProcessID);
            if (process != null)
            {
                // Wait for target process to exit
                process.WaitForExit(15000);

                // Kill target process if not exited by time limit
                if (!process.HasExited)
                    KillProcess(options.ProcessID);
            }

            // Copy & replace the downloaded files
            CopyFiles(options.Source, options.Destination);

            // Start the target process
            ProcessUtility.RunProcess(options.ApplicationPath, waitForExit: false);

            return 1;
        }

        static void CopyFiles(string source, string destination)
        {
            try
            {
                var sourceFileNames = Directory.GetFiles(source);

                foreach (var sourceFileName in sourceFileNames)
                {
                    var fileName = Path.GetFileName(sourceFileName);
                    var destFileName = Path.Combine(destination, fileName);

                    File.Copy(sourceFileName, destFileName, true);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static Process GetProcess(int processID)
        {
            Process process = null;
            try
            {
                process = Process.GetProcessById(processID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return process;
        }

        static void KillProcess(int processID)
        {
            ProcessUtility.RunProcess("taskkill", "/PID " + processID);
        }
    }
}
