﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Threading.Tasks;
using System.IO;

using TestAgent.Client;
using Command;

public class EditorTest
{
    [MenuItem("TestRunner/Test")]
    public static async Task Test()
    {
        var response = await SendRPC.Command (CommandRequest.Types.Type.RestartOs);
        var message = response.Message;
        Debug.Log(message);
    }

    [MenuItem("TestRunner/Upload File")]
    public static async Task UploadFile()
    {
        var fileName = @"c:\temp\lastpass_x64.exe";
        var destination = @"d:\Upload";
        var response = await SendRPC.UploadFile(fileName, destination);
        Debug.Log(response);
    }

    [MenuItem("TestRunner/Update Server")]
    public static async Task UpdateServer()
    {
        SendRPC.UpdateServer();
    }
}
